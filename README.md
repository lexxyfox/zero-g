# Zero G

Silly chromium-based web browser.

## Goals / Philosophies

* All telemetry / reporting / sending data to a third party (if any) should be opt-in only.
    * Special hate towards Google and Microsoft - they're outright banned, not even opt-in.
* Built-in ad blocker that supports ABP / ublock ("URL Blocklist") block lists.
* Ability to download extensions from Chrome web store.
* [Component build](https://chromium.googlesource.com/chromium/src/+/master/docs/component_build.md) - compile to many library files instead of one, two, or three files. 
* Use system libraries where possible.
* Ability to ~~be a pirate~~ watch non-free videos / multimedia, unless user opt-outs.
* FLoC = big nope
* Don't be fixated on a specific display server - move with the times, as long as it's not Canonical's Mir.
* Avoid Python 2 (/ whatever versions of Python are depreciated at the time) if at all possible.
* Avoid any Python where not needed. 
* **My** target OS preference order (can change if/when other developers are onboarded):
    1. Debians / Ubuntus
    1. OpenSUSE
    1. Arch
    1. Fedora / Centos (maybe? May move up or down the list)
    1. AppImage
    1. GNU Guix
    1. ~big gap here~
    1. M$ Windblows
    1. Android
* Eventually in the future / wishful thinking: Free browser sync between at least other Zero G browsers
    * And Chrome? And Firefox???
