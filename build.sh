#!/usr/bin/env bash

CHROMIUM_VER='89.0.4389.114'
UNGOOGLED_VER="$CHROMIUM_VER-1"
ROOT_DIR='ungoogled'
MAIN_REPO="$ROOT_DIR/ungoogled-chromium" # path to ungoogled chromium source
SRC_DIR="$ROOT_DIR/build/src" # path to google chromium source
GN_DIR="gn_build" # where gn gets compiled to
OUT_DIR="build" # where gn puts the ninja files. Was "$SRC_DIR/out/Default"

# PREREQUISITES
# OpenSUSE
#   sudo zypper in crudini cups-devel lld11 llvm11 clang11 ninja gperf 'pkgconfig(alsa)' 'pkgconfig(atk-bridge-2.0)' 'pkgconfig(gbm)' 'pkgconfig(gtk+-3.0)' 'pkgconfig(krb5)' 'pkgconfig(lcms2)' 'pkgconfig(libpci)' 'pkgconfig(libpulse)' 'pkgconfig(libturbojpeg)' 'pkgconfig(libva)' 'pkgconfig(nss)' 'pkgconfig(xkbcommon)' 'pkgconfig(xshmfence)'
# Ubuntu
#   sudo apt install clang crudini gperf llvm ninja-build nodejs pkgconf libxshmfence-dev
#   TEMP_BIN="$(mktemp -d)"
#   trap "rm -rf '$TEMP_BIN'" EXIT
#   ln -s "$(which python2)" "$TEMP_BIN"/python
#   PATH="$TEMP_BIN:$PATH"
#   sudo ln -s "$(which node)" /usr/bin/nodejs
#   #mkdir -p "$SRC_DIR/third_party/node/linux/node-linux-x64/bin"
#   #ln -s "$(which node)" "$_"

export AR=${AR:=llvm-ar}
export NM=${NM:=llvm-nm}
export CC=${CC:=clang}
export CXX=${CXX:=clang++}

# breaks on opensuse...
# C_RES_FLAG="${C_RES_FLAG:="-resource-dir=$("$CC" --print-resource-dir)"}"

export CXXFLAGS+="$C_RES_FLAG '$(pkgconf --cflags xkbcommon | xargs)'"
export CPPFLAGS+="$C_RES_FLAG"
export CFLAGS+="$C_RES_FLAG"



git clone --depth 1 \
  https://github.com/ungoogled-software/ungoogled-chromium-portablelinux.git \
  "$ROOT_DIR"
git clone -b "$UNGOOGLED_VER" -c advice.detachedHead=false --depth 1 \
  https://github.com/Eloston/ungoogled-chromium.git \
  "$MAIN_REPO"
# Note: I'd like to use git to download chromium, but it requires gclient.
# Meanwhile, the tar file contains everything we need...
mkdir -p "$SRC_DIR"
curl "https://commondatastorage.googleapis.com/chromium-browser-official/chromium-${CHROMIUM_VER}.tar.xz" \
  | tar xJC "$SRC_DIR" -X "$MAIN_REPO/pruning.list" --str 1 --exclude-vcs # does --exclude-vcs work?
find "$SRC_DIR" -empty -type d -delete # deletes empty folders

# Yes, I've heard of quilt. But have you tried it?
(
  (cd "$MAIN_REPO/patches"; xargs cat < series)
  (cd "$ROOT_DIR/patches"; xargs cat < series)
  (cd "patches"; xargs cat < series)
) | patch -Elp1 -d "$SRC_DIR" --no-backup-if-mismatch

# futurize -0 and -1 don't work...
# { futurize -1wj $(nproc) --no-diffs "$SRC_DIR" 2>&1 1>&3 | grep -v '^RefactoringTool: No changes to' 1>&2 } 3>&1

# Let's skip the domain substitution for now...

mkdir -p "$OUT_DIR"
cat "$MAIN_REPO/flags.gn" "$ROOT_DIR"/flags.*.gn > "$_/args.gn"
# TODO: enable optimize_webui later, has problems with node
# TODO enable: is_component_ffmpeg optimize_webui pdf_enable_xfa rtc_build_json rtc_build_libevent rtc_build_libvpx rtc_build_opus rtc_build_ssl rtc_use_gtk rtc_use_h264 use_system_harfbuzz
crudini --merge --inplace "$OUT_DIR/args.gn" < args.ini

python3 "$SRC_DIR/tools/gn/build/gen.py" --no-last-commit-position --no-static-libstdc++ --out-path "$GN_DIR"
cp "$SRC_DIR/tools/gn/bootstrap/last_commit_position.h" "$GN_DIR"
ninja -k0 -C "$GN_DIR" gn

"$GN_DIR/gn" gen --root="$SRC_DIR" "$OUT_DIR" --fail-on-unused-args
# "$GN_DIR/gn" args --root="$SRC_DIR" "$OUT_DIR" --list > args
ninja -k0 -C "$OUT_DIR" chrome chrome_sandbox chromedriver



ARCHIVE_OUTPUT="ungoogled-chromium_$UNGOOGLED_VER.$(cat $ROOT_DIR/revision.txt)_linux.tar.xz"

"$MAIN_REPO/utils/filescfg.py" \
	-c "$SRC_DIR/chrome/tools/build/linux/FILES.cfg" \
	--build-outputs "$OUT_DIR" \
	archive \
	-o "$ARCHIVE_OUTPUT" \
	-i "$ROOT_DIR/tar_includes/README" \
	-i "$OUT_DIR/libminigbm.so" # use_system_minigbm=false
